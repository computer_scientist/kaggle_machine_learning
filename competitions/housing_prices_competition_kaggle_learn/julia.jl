# Import necessary libraries
using CSV
using DataFrames
using DecisionTree
using MLJ
using MLBase

# Path of the file to read
iowa_file_path::String = "./data/train.csv"

# Read file
home_data::DataFrame = CSV.read(iowa_file_path, DataFrame)

# Create the list of features
features_1::Vector{String} = [
    "LotArea", "YearBuilt", "1stFlrSF", "2ndFlrSF", "FullBath", "BedroomAbvGr", "TotRmsAbvGrd"
]

features_2::Vector{String} = [
    "OverallQual", "GrLivArea", "GarageCars", "GarageArea", "TotalBsmtSF", "1stFlrSF", "FullBath", "TotRmsAbvGrd", "YearBuilt", "YearRemodAdd"
]

# Select data corresponding to features in features
X::Matrix{Int64} = Matrix(home_data[!, features_2])
y::Vector{Int64} = Vector(home_data[!, :SalePrice])

# Split the data into training and validation sets using indices
train_indices::Vector{Int64}, val_indices::Vector{Int64} = MLJ.partition(eachindex(y), 0.75, shuffle=true, rng=1)

# Extract training and validation data on indices
train_X::Matrix{Int64} = X[train_indices, :]
train_y::Vector{Int64} = y[train_indices]
val_X::Matrix{Int64} = X[val_indices, :]
val_y::Vector{Int64} = y[val_indices]

# Define the model with the default settings
random_forest_model::RandomForestRegressor = RandomForestRegressor(rng=1)

# Fit the model
DecisionTree.fit!(random_forest_model, train_X, train_y)

# Calculate predictions on the validation set
random_forest_val_predictions::Vector{Float64} = DecisionTree.predict(random_forest_model, val_X)

# Calculate the mean absolute error using MLBase
random_forest_val_mae::Float64 = MLJ.mean_absolute_error(random_forest_val_predictions, val_y)

# Display results
println("Validation MAE for Random Forest Model: $random_forest_val_mae")

# Path to file to read for test data
test_data_path::String = "./data/test.csv"

# Read file
test_data::DataFrame = CSV.read(test_data_path, DataFrame)

# Define a random forest model
random_forest_on_full_data::RandomForestRegressor = RandomForestRegressor()

# Fit the model
DecisionTree.fit!(random_forest_on_full_data, X, y)

# Set test_X from features
test_X::Matrix{Int64} = Matrix(test_data[!, features_2])

# Calculate predictions on the validation set
test_predictions::Vector{Float64} = DecisionTree.predict(random_forest_on_full_data, test_X)


# # Write outputs
output = DataFrame(Id = test_data[!, "Id"], SalePrice=test_predictions)
CSV.write("./results/submission_julia.csv", output)
println("Your submission was successfully saved")
