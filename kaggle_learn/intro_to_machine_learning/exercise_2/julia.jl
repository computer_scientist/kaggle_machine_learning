# Import necessary libraries
using CSV
using DataFrames
using DecisionTree

function custom_print(predictions::Vector{Float64}; num_show::Int64=3)
    n::Int64 = length(predictions)
    
    if n > 2 * num_show
        # Show the first and last `num_show` elements, separated by ellipsis
        first_part::String = join(predictions[1:num_show], ", ")
        last_part::String = join(predictions[end-num_show+1:end], ", ")
        println("[$first_part, ..., $last_part]")
    else
        # If the array is not large enough, just print it normally
        println(predictions)
    end
end


# Path of the file to read
iowa_file_path::String = "./data/train.csv"

# Read file
home_data::DataFrame = CSV.read(iowa_file_path, DataFrame)

# Create the list of features
feature_names::Vector{String} = ["LotArea", "YearBuilt", "1stFlrSF", "2ndFlrSF", "FullBath", "BedroomAbvGr", "TotRmsAbvGrd"]

# Select data corresponding to features in feature_names
X::Matrix{Int64} = Matrix(home_data[!, feature_names])
y::Vector{Int64} = Vector(home_data[!, :SalePrice])

# For model reproducibility, set a numeric value for rng when specifying the model
iowa_model::DecisionTreeRegressor = DecisionTreeRegressor(max_depth=10, min_samples_leaf=5, min_samples_split=2, rng=1)

# Fit the model
fit!(iowa_model, X, y)

# Make predictions
predictions::Vector{Float64} = predict(iowa_model, X)

# Print predictions
custom_print(predictions)