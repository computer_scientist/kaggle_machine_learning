# Import necessary libraries
using CSV
using DataFrames
using DecisionTree
using MLJ
using MLBase

# Path of the file to read
iowa_file_path::String = "./data/train.csv"

# Read file
home_data::DataFrame = CSV.read(iowa_file_path, DataFrame)

# Create the list of features
feature_names::Vector{String} = [
    "LotArea", "YearBuilt", "1stFlrSF", "2ndFlrSF", "FullBath", "BedroomAbvGr", "TotRmsAbvGrd"
]

# Select data corresponding to features in feature_names
X::Matrix{Int64} = Matrix(home_data[!, feature_names])
y::Vector{Int64} = Vector(home_data[!, :SalePrice])

# For model reproducibility, set a numeric value for rng when specifying the model
iowa_model::DecisionTreeRegressor = DecisionTreeRegressor()

# Fit the model
DecisionTree.fit!(iowa_model, X, y)

# Make predictions
predictions::Vector{Float64} = DecisionTree.predict(iowa_model, X)

# Display prediction and actual values
println("First in-sample predictions: ", DecisionTree.predict(iowa_model, X[1:5, :]))
println("Actual target values for those homes: ", y[1:5])

# Split the data into training and validation sets using indices
train_indices::Vector{Int64}, val_indices::Vector{Int64} = MLJ.partition(eachindex(y), 0.75, shuffle=true, rng=1)

# Extract training and validation data based on indices
train_X::Matrix = X[train_indices, :]
train_y::Vector{Int64} = y[train_indices]
val_X::Matrix{Int64} = X[val_indices, :]
val_y::Vector{Int64} = y[val_indices]

# Instantiate the DecisionTreeRegressor
iowa_model::DecisionTreeRegressor = DecisionTreeRegressor()

# Fit the model
DecisionTree.fit!(iowa_model, train_X, train_y)

# Predict with all validation observations
val_predictions::Vector{Float64} = DecisionTree.predict(iowa_model, val_X)

# Display prediction and actual values
println("Second in-sample predictions: ", DecisionTree.predict(iowa_model, X[1:5, :]))
println("Actual target values for those homes: ", y[1:5])

# Calculate MAE from MLBase package
val_mae::Float64 = mae(val_y, val_predictions)

# Display Mean Absolute Error
println("Mean Absolute Error: ", val_mae)