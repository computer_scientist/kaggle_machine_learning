# Import necessary libraries
using CSV
using DataFrames
using DecisionTree
using MLJ
using MLBase

# Path of the file to read
iowa_file_path::String = "./data/train.csv"

# Read file
home_data::DataFrame = CSV.read(iowa_file_path, DataFrame)

# Create the list of features
feature_names::Vector{String} = [
    "LotArea", "YearBuilt", "1stFlrSF", "2ndFlrSF", "FullBath", "BedroomAbvGr", "TotRmsAbvGrd"
]

# Select data corresponding to features in feature_names
X::Matrix{Int64} = Matrix(home_data[!, feature_names])
y::Vector{Int64} = Vector(home_data[!, :SalePrice])

# For model reproducibility, set a numeric value for rng when specifying the model
iowa_model::DecisionTreeRegressor = DecisionTreeRegressor()

# Fit the model
DecisionTree.fit!(iowa_model, X, y)

# Make predictions
predictions::Vector{Float64} = DecisionTree.predict(iowa_model, X)

# Display prediction and actual values
println("First in-sample predictions: ", DecisionTree.predict(iowa_model, X[1:5, :]))
println("Actual target values for those homes: ", y[1:5])

# Split the data into training and validation sets using indices
train_indices::Vector{Int64}, val_indices::Vector{Int64} = MLJ.partition(eachindex(y), 0.75, shuffle=true, rng=1)

# Extract training and validation data based on indices
train_X::Matrix = X[train_indices, :]
train_y::Vector{Int64} = y[train_indices]
val_X::Matrix{Int64} = X[val_indices, :]
val_y::Vector{Int64} = y[val_indices]

# Instantiate the DecisionTreeRegressor
iowa_model::DecisionTreeRegressor = DecisionTreeRegressor()

# Fit the model
DecisionTree.fit!(iowa_model, train_X, train_y)

# Predict with all validation observations
val_predictions::Vector{Float64} = DecisionTree.predict(iowa_model, val_X)

# Display prediction and actual values
println("Second in-sample predictions: ", DecisionTree.predict(iowa_model, X[1:5, :]))
println("Actual target values for those homes: ", y[1:5])

# Calculate MAE from MLBase package
val_mae::Float64 = mae(val_y, val_predictions)

# Display Mean Absolute Error
println("Mean Absolute Error: ", val_mae)

function get_mae(max_leaf_nodes, train_X, val_X, train_y, val_y)
    model = DecisionTreeRegressor(max_depth=max_leaf_nodes, rng=0)
    DecisionTree.fit!(model, train_X, train_y)
    preds_val = DecisionTree.predict(model, val_X)
    meanabs = mae(val_y, preds_val)
    println("$max_leaf_nodes: $meanabs")
    return meanabs
end

# List of possible max leaf nodes
candidate_max_leaf_nodes = [5, 25, 50, 100, 250, 500]

# Calculate scores using a dictionary comprehension in Julia
scores = Dict(leaf_size => get_mae(leaf_size, train_X, val_X, train_y, val_y) for leaf_size in candidate_max_leaf_nodes)

# Find the key of the best tree size with the minimum MAE
best_tree_size = collect(keys(scores))[argmin(collect(values(scores)))]

# Display the best tree size
println("Best tree size: $best_tree_size")

# Instantiate the DecisionTreeRegressor
final_model::DecisionTreeRegressor = DecisionTreeRegressor(max_depth=best_tree_size, rng=1)

# Fit the model
DecisionTree.fit!(final_model, X, y)

# Predict with all validation observations
final_predictions::Vector{Float64} = DecisionTree.predict(final_model, val_X)

# Calculate MAE from MLBase package
final_mae::Float64 = mae(val_y, final_predictions)

# Display Mean Absolute Error
println("Mean Absolute Error: ", final_mae)